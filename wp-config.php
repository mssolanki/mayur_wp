<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mayur_wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'k4_|D{$1Oou:s/pzxW}IwS/[oOBZS|>,nzY};K&mWt,5:y(:wrii0ql,=RCz|fI1' );
define( 'SECURE_AUTH_KEY',  'ZKR>(7J:SK`4K@~E</TI ?(~+(N]:ae; S~N^u7IuD BvIO;vwScg:i&oqrXUrHa' );
define( 'LOGGED_IN_KEY',    'p+$[(O=2/aE`0Zr!2=1ekV~o]YyUn4A2ogBZ]4|!<8NDfC_2~[W|>ym}IDT}U${t' );
define( 'NONCE_KEY',        'F/X5$||j{$cI7W5RY-WmST<:rn%m;~2 {qg1f M=!)w0+8nG^LbK:JQOGD$L?R)q' );
define( 'AUTH_SALT',        '~uh(;//SY*RAe(L%/a{*9h{J^L ?lOgG}vN^?kqPF{1Z=5{nv&`!LNMCv^ikO!Dc' );
define( 'SECURE_AUTH_SALT', 'tbJgM;:[hOB$;zTeAMI``4qMXvo0Pz.xhAxNfyC7^+q{lXVgd,|Z-HzU^>8!% at' );
define( 'LOGGED_IN_SALT',   '{| 4wpaT4vNvj0XlqA|G3Nf=VX09zdON{0VKPrK]#$X=L%ZbYpW8OjN`N*[pYJZg' );
define( 'NONCE_SALT',       '+aH:mP^@>=&9*2xOkBm=}C~Eeo)4TDO-oDgCtgC[yW*]b6M4<y$$CF^-B:_MJP]#' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
