<?php
/*
Template Name: Home Page
*/
get_header();

?>
<section class="site_sec">

    <div class="top_sec">
        <div class="wrapper">
            <div class="circle_img">
                <?php 
                    $page_id     = get_queried_object_id();
                    //echo 'page id:'.$page_id;
                    $value = get_field( "row1", $page_id );
                    if( $value ) {
                    echo $value;
                    } else {
                    echo 'empty';
                    }
                ?>

            </div>
        </div><!--end of wrapper-->
    </div><!--end of top_sec-->

    <div class="row2">
        <div class="row2top"></div>
        <div class="wrapper">
            <?php 
                    $page_id     = get_queried_object_id();
                    //echo 'page id:'.$page_id;
                    $value = get_field( "row2", $page_id );
                    if( $value ) {
                    echo $value;
                    } else {
                    echo 'empty';
                    }
                ?>
        </div>
    </div>

    <div class="row3">
        <div class="wrapper">
            <?php 
                    $page_id     = get_queried_object_id();
                    //echo 'page id:'.$page_id;
                    $value = get_field( "row3", $page_id );
                    if( $value ) {
                    echo $value;
                    } else {
                    echo 'empty';
                    }
                ?>
        </div>
    </div>




    <div class="row4">
        <div class="corner_l"></div>
        <div class="wrapper ">
                <div class="row4_sec clearfix">
                    <?php 
                    $page_id     = get_queried_object_id();
                    $value = get_field( "row4", $page_id );
                    if( $value ) {
                    echo $value;
                    } else {
                    echo 'empty';
                    }
                    ?>
                </div>
        </div>
    </div>




    <!--
    <div class="row4">
        <div class="corner_l"></div>
        <div class="wrapper ">
                <div class="row4_sec clearfix">
                    <div class="row4_sec_l">
                        <h3>practice</h3> 
                        <h2>dynamics</h2>
                        <h4>1. <span>HATHA</span> YOGA</h4>
                        <p>Hatha is the most classic yoga. Its origin is in the Raya Yoga of Patanjali and other classical texts such as 
                        Bhagavad Gita and Hatha Yoga 
                        Pradipika.</p>
                        <div class="hr"></div>
                        <span>10 modules <br/>
                        divided into 7 weekends</span>
                        <div class="demo_text">
                        <p class="fl">Start
                        <span>April 15</span>
                        <span>20.00 hs</span></p>
                        <p class="fr">Price
                        <span>$900</span></p>
                        </div>      
                    </div>
                    <div class="row4_sec_r">
                        <img src="<?php //echo get_template_directory_uri();?>/images/yoga4.jpg">
                    </div>
                </div>
        </div>
    </div>

    <div class="row4">
        <div class="corner_r"></div>
        <div class="wrapper ">
            <?php 
                    /*$page_id     = get_queried_object_id();
                    $value = get_field( "row3", $page_id );
                    if( $value ) {
                    echo $value;
                    } else {
                    echo 'empty';
                    }*/
                ?>
                <div class="row4_sec clearfix">
                    <div class="row4_sec_l">
                        
                        <img src="<?php //echo get_template_directory_uri();?>/images/yoga5.jpg">      
                    </div>
                    <div class="row4_sec_r">
                        <h3>Positions</h3> 
                        <h2>dynamics</h2>
                        <h4>2.  <span>Kundalini</span> YOGA</h4>
                        <p>Kundalini is the most classic yoga. Its 
origin is in the Raya Yoga of Patanjali and other classical texts such as 
Bhagavad Gita and Hatha Yoga 
Pradipika.</p>
                        <div class="hr"></div>
                        <span>17 modules <br/>
                        divided into 5 weekends</span>
                        <div class="demo_text">
                        <p class="fl">Start
                        <span>April 15</span>
                        <span>20.00 hs</span></p>
                        <p class="fr">Price
                        <span>$900</span></p>
                        </div>
                    </div>
                </div>
        </div>
    </div> -->



    <div class="row4">
        <div class="corner_r"></div>
        <div class="wrapper ">
            
                <div class="row4_sec clearfix">

                    <?php 

                    $page_id     = get_queried_object_id();
                    $value = get_field( "row5", $page_id );

                    if( $value ) {
                        echo $value;
                    } else {
                        echo 'empty';
                    }

                    ?>
                </div>
        </div>
    </div>


    <div class="row6">
        <div class="wrapper">
            <?php 

                    $page_id     = get_queried_object_id();
                    $value = get_field( "row6", $page_id );

                    if( $value ) {
                        echo $value;
                    } else {
                        echo 'empty';
                    }

             ?>
        </div>
    </div><!--end of row6-->

    <!--
    <div class="row6">
        <div class="wrapper">
            <span>HEALTHY</span>
            <h2>life</h2>
            <div class="widget">
                <ul>
                    <li>
                        <a href="#"><img src="<?php //echo get_template_directory_uri();?>/images/wid1.png"></a>
                        <a href="#">EXERCISE</a>
                        <p>Lorem ipsum dolor sit amet consectetur adipiscing, elit molestie posuere</p>
                    </li>
                    <li>
                        <a href="#"><img src="<?php //echo get_template_directory_uri();?>/images/wid2.png"></a>
                        <a href="#">FEEDING</a>
                        <p>Lorem ipsum dolor sit amet consectetur adipiscing, elit molestie posuere</p>
                    </li>
                    <li>
                        <a href="#"><img src="<?php //echo get_template_directory_uri();?>/images/wid3.png"></a>
                        <a href="#">EXERCISE</a>
                        <p>Lorem ipsum dolor sit amet consectetur adipiscing, elit molestie posuere</p>
                    </li>
                </ul>
            </div>
        </div>
    </div><!--end of row6-->


    <div class="row2">
        <div class="wrapper">
            <?php 
                    $page_id     = get_queried_object_id();
                    //echo 'page id:'.$page_id;
                    $value = get_field( "row2", $page_id );
                    if( $value ) {
                    echo $value;
                    } else {
                    echo 'empty';
                    }
                ?>
        </div>
    </div>



    <div class="row8">
                 <?php 
                    //echo '<br/>mayur solanki<br/>';
                    $page_id     = get_queried_object_id();
                    $value = get_field( "row7", $page_id );
                    if( $value ) {
                    echo $value;
                    } else {
                    echo 'empty';
                    }
                ?>
    </div><!--end of row8-->




    <!--
    <div class="row8">
                <span class="v_left">initiation</span>
                <span class="v_right">Workshop</span>
        <div class="wrapper ">
                <div class="row4_sec clearfix">
                    <div class="row4_sec_l">
                        <h2>yoga initiation</h2>
                        <h3>workshop</h3>
                        <p>TRY YOUR FIRST FREE</p>
                        <p>YOGA CLASS</p>
                        <span>22.12.2020<br/>
                        24.03. 2021</span>
                              
                    </div>
                    <div class="row4_sec_r">
                       <img src="<?php //echo get_template_directory_uri();?>/images/yoga6.jpg">
                    </div>
                </div>
        </div><!--end of wrapper-->
    <!--</div><!--end of row8-->



<?php 
/*
echo 'mayur'; 

print_r(the_widget('Footer#1')) ;

the_widget( 'WP_Widget_Archives');

the_widget( 'WP_Widget_Recent_Posts' );
wp_reset_postdata();

echo '<br/><h1>Here get the post function execute.</h1><br/>';
$args = array(
  'numberposts' => 2,
  'post_type'   => 'post'
);
$post=get_posts(  $args );

foreach($post as $post1){
	
echo $post1->post_title.'<br/>';
echo $post1->post_type.'<br/>';
echo $post1->post_author.'<br/>';
echo $post1->post_status.'<br/>';
echo $post1->post_content.'<br/>';
echo $post1->post_excerpt.'<br/>';
echo $post1->post_date.'<br/>';

}
//wp_pagenavi(array( 'query' => $query ));

?> 

<?php wp_reset_postdata();

echo '<br/><h1>Here get the page function execute.</h1><br/>';

// set the "paged" parameter (use 'page' if the query is on a static front page)
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : '1';
$args = array (
	'posts_per_page' => 1,
    'paged'                  => $paged,
    'post_type'              => 'page',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {


    while ( $query->have_posts() ) {
        $query->the_post();
        echo '<div class="news-item">';
            // post stuff here
                echo '<h1 class="page-title screen-reader-text">' . the_title() . '</h1>';
                echo the_content();
        echo '</div>';
    }

    

    wp_pagenavi(array( 'query' => $query ));

} else {
    // no posts found
    echo '<h1 class="page-title screen-reader-text">No Posts Found</h1>';
}

// Restore original Post Data
wp_reset_postdata();
?>



<?php wp_reset_postdata();

echo '<br/><h1>Here get the post function again execute.</h1><br/>';

// set the "paged" parameter (use 'page' if the query is on a static front page)
$paged1 = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : '1';
$args2 = array (
    'posts_per_page' => 1,
    'paged'                  => $paged1,
    'post_type'              => 'post',
);

// The Query
$query2 = new WP_Query( $args2 );

// The Loop
if ( $query2->have_posts() ) {


    while ( $query2->have_posts() ) {
        $query2->the_post();
        echo '<div class="news-item">';
            // post stuff here
                echo '<h1 class="page-title screen-reader-text">' . get_the_title() . '</h1>';
                echo the_content();
                echo $query->post_title.'<br/>';
        echo '</div>';
    }

    

    wp_pagenavi(array( 'query' => $query2 ));

} else {
    // no posts found
    echo '<h1 class="page-title screen-reader-text">No Posts Found</h1>';
}

// Restore original Post Data
wp_reset_postdata();

echo '<br/>Get the categories<br/>';

$categories = get_categories( array(
    'orderby' => 'name',
    
) );
 
foreach ( $categories as $category ) {
    printf( '<a href="%1$s">%2$s</a><br />',
        esc_url( get_category_link( $category->term_id ) ),
        esc_html( $category->name )
    );
}
wp_reset_postdata();

echo '<br/>Get the tags<br/>';
$posttags = get_the_tags(array(
    'orderby' => 'name',
    
));
if ($posttags) {
  foreach($posttags as $tag) {
    echo $tag->name . ' '; 
  }
}



$tag_list = get_the_tag_list( '<ul><li>', '</li><li>', '</li></ul>' );
 
if ( $tag_list && ! is_wp_error( $tag_list ) ) {
    echo $tag_list;
}


wp_reset_postdata();

*/
?>

</section><!--end of site_sec-->

<?php 
get_footer();
?>