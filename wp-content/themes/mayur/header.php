<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/style.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jsval.js"></script>
		<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.js"></script>
		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

		<?php
		wp_body_open();
		?>

<?php //echo site_url();?>

	<header>
		<div class="top_line"></div>
		
		<div class="wrapper">
		<div class="head">
			<div class="logo">
				<a href="<?php echo site_url();?>">
				<img src="<?php echo get_template_directory_uri();?>/images/logo.png" alt="Logo">
				</a>
			</div>

			<nav class="mainmenu" id="myTopnav">
			<?php 
			//wp_nav_menu(array('menu'   => 'second nav'));
			
			?>
			<?php wp_nav_menu(); ?><a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
			</nav>
		</div><!-end of head-->
	</div><!-end of t_width-->

	</header>