<?php
/*
Template Name: Contact
*/
?>

<?php get_header() ?>

<?php
if(isset($_POST['submitted'])) {
	if(trim($_POST['contactName']) === '') {
		$nameError = 'Please enter your name.';
		$hasError = true;
	} else {
		$name = trim($_POST['contactName']);

		//echo $name;
	}

	if(trim($_POST['email']) === '')  {
		$emailError = 'Please enter your email address.';
		$hasError = true;
	} else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['email']))) {
		$emailError = 'You entered an invalid email address.';
		$hasError = true;
	} else {
		$email = trim($_POST['email']);
		//echo $email;
	}

	if(trim($_POST['comments']) === '') {
		$commentError = 'Please enter a message.';
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($_POST['comments']));
		} else {
			$comments = trim($_POST['comments']);

		}
	}


	//echo '<br/>'.$name.'<br/><br/>'.$email.'<br/><br/>'.$comments.'<br/>';



	if(!isset($hasError)) {
		$emailTo = get_option('tz_email');
		if (!isset($emailTo) || ($emailTo == '') ){
			$emailTo = get_option('admin_email');
		}
		$subject = '[PHP Snippets] From '.$name;
		$body = "Name: $name \n\nEmail: $email \n\nComments: $comments";
		$headers = 'From: '.$name.' <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;

		wp_mail($emailTo, $subject, $body, $headers);
		$emailSent = true;
	}





   //echo get_bloginfo('admin_email');
   

   global $phpmailer; // define the global variable
if ( !is_object( $phpmailer ) || !is_a( $phpmailer, 'PHPMailer' ) ) { // check if $phpmailer object of class PHPMailer exists
	// if not - include the necessary files
	require_once ABSPATH . WPINC . '/class-phpmailer.php';
	require_once ABSPATH . WPINC . '/class-smtp.php';
	$mail = new PHPMailer( true );
}





    if (empty($errors)) {
    //$mail = new PHPMailer(); //defaults to using php "mail()"; the true param means it will throw exceptions on errors, which we need to catch
try {

    $mail->SMTPDebug = 0;                               // Enable verbose debug output
    $mail->isSMTP();                                    // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
    /*$mail->SMTPOptions = array(
'ssl' => array(
'verify_peer' => false,
'verify_peer_name' => false,
'allow_self_signed' => true
)
);*/
    $mail->Username = 'testingphpm@gmail.com';           // SMTP username
    $mail->Password = 'testing@123';                       // SMTP password
    $mail->SMTPAuth = true;
	$mail->SMTPAutoTLS = true; 
	$mail->Port = 587;                                // TCP port to connect, tls=587, ssl=465
    $mail->From = 'testingphpm@gmail.com';
    $mail->FromName = $name;
    $mail->addAddress(get_bloginfo('admin_email'));     // Add a recipient
    $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
    $mail->isHTML(false);                                  // Set email format to HTML
    $mail->Subject = $comments;
    $mail->Body    = 'Name:'.$name.'<br/>Email id:'.$email.'<br/>Message:'.$comments;
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
    if(!$mail->send()) {
        echo 'Message could not be sent.';
        echo 'Mailer Error: ' . $mail->ErrorInfo;
    } else {
        echo 'Message has been sent';
        echo '<div style="color:green">Message has been sent</div>';
    }
    $errors[] = "Send mail sucsessfully";
} catch (phpmailerException $e) {
    $errors[] = $e->errorMessage(); //Pretty error messages from PHPMailer
} catch (Exception $e) {
    $errors[] = $e->getMessage(); //Boring error messages from anything else!
}
}





} ?>

<section class="site_sec">
	<div class="row2">
        <div class="wrapper">
        	<h2><?php echo get_the_title(); ?></h2>
        	<p><?php echo get_the_content(); ?></p>
			

        	<form action="<?php the_permalink(); ?>" id="contactForm8" method="post">
	<ul>
		<li>
			<label for="contactName">Name:</label>
			<input type="text" name="contactName" id="contactName" value="" required>
		</li>
		<li>
			<label for="email">Email</label>
			<input type="text" name="email" id="email" value="" required>
		</li>
		<li>
			<label for="commentsText">Message:</label>
			<textarea name="comments" id="commentsText" rows="20" cols="30" required></textarea>
		</li>
		<li>
			<input type="submit" name="submitted" id="submitted" value="submit" />
		</li>
	</ul>
	
</form>




		</div><!-- #wrapper -->
	</div><!-- #row2 -->
</section>
<?php //get_sidebar() ?>



<?php get_footer() ?>


<script>
	$(document).ready(function(){
	$("#contactForm8").validate({


			rules: {
			contactName : {
			required: true,

			},
			email: {
			required: true,
			email: true
			},
			comments: {
			required: true
			},

			},
			messages : {

			email: {
			email: "The email should be in the format: abc@domain.tld"
			},
			}

		});
	
	});
</script>