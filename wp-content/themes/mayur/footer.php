<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
			<footer class="main_footer">
				<div class="footer_line"></div>
				<div class="wrapper clearfix">

					<div class="footer_1">
							<?php if ( is_active_sidebar( 'footer-top' ) ) { ?>

							<aside class="after-content widget-area full-width" role="complementary">
							<?php dynamic_sidebar( 'footer-top' ); 
							?>
							</aside>

							<?php }?>
					</div><!-- .footer_1 -->
					<div class="footer_2">

						<div class="row4_sec_l">
							<?php if ( is_active_sidebar( 'footer-left' ) ) { ?>

							<aside class="after-content widget-area full-width" role="complementary">
							<?php dynamic_sidebar( 'footer-left' ); 
							?>
							</aside>

							<?php }?>
						

						</div>
						<div class="row4_sec_r">


							<?php if ( is_active_sidebar( 'footer-right' ) ) { ?>

							<aside class="after-content widget-area full-width" role="complementary">
							<?php dynamic_sidebar( 'footer-right' ); 
							?>
							</aside>

							<?php }?>
							
						</div>

					</div><!-- .footer_2 -->

				</div><!-- .wrapper -->


				<div class="footer_bg"></div>

			</footer><!-- #site-footer -->

		<?php wp_footer(); ?>

<script>

function myFunction() {
  var x = document.getElementById("menu-main-menu");
  if (x.className === "menu") {
    x.className += " responsive";
  } else {
    x.className = "menu";
  }
}
</script>
<script>
//var txt1 = "<i class="fas fa-plus-square"></i>";
//var txt1 = "<i class="fas fa-minus-square"></i>";
//$(".menu li").append(txt1);
var txt1 = "<i class='fa fa-minus'></i>";
var txt2="<i class='fa fa-plus'></i>"
jQuery(document).ready(function(){

    // Proceed only if children with your submenu class are present
    if ($("ul.menu li").find('.menu-item-has-children')) {
       $(".menu li.menu-item-has-children").append(txt2);
       
       //$(".fa-plus").toggleClass('toggle');
			
				$("i.fa").click(function(){
					$(this).parent().toggleClass('active');
					$(this).siblings().toggleClass('block');
					$(this).toggleClass('fa-plus');
					$(this).toggleClass('fa-minus');
					/*$(this).removeClass('fa-plus');
					$(this).addClass('fa-minus');*/
				});
				
				/*jQuery("i.fa-minus").click(function(){
					
					$(this).removeClass('fa-minus');
					$(this).addClass('fa-plus');
				});*/
    }

});


</script>

	</body>
</html>
